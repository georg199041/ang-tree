Objects tree with angular!
To run the static server you need to have nodejs be installed.

1) run in console git clone https://gitlab.com/georg199041/ang-tree.git
2) go to the ang-tree directory
3) run node server.js to start static server, go to http://localhost:3000/

You will be able add, edit, delete tree nodes.

Also there are several tests you can run them, you will need protractor and webdriver-manager
1) Ansure you have protractor istalled (protractor --version), otherwise run: 
sudo npm install -g protractor
2) Then download the necessary binaries with: sudo webdriver-manager update
3) Start up a server: webdriver-manager start
4) In a different terminal move to tests/e2e folder and run: protractor conf.js