'use strict';

angular.module('RecursiveObjectsTree').directive('recursiveObjectsTreeDirective', function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'modules/RecursiveObjectsTree/RecursiveObjectsTreeDirective.html'
	}
});