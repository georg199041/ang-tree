'use strict';

angular.module('RecursiveObjectsTree').
	controller('RecursiveObjectsTreeController', ['$scope', function($scope) {
		$scope.tree = JSON.parse(localStorage.getItem('recursiveObjectTree')) || [{name: 'node', nodes: []}];

		$scope.delete = function(data) {
			data.nodes = [];
		};
		$scope.add = function(data) {
			var post = data.nodes.length + 1;
			var newName = 'node-' + post;
			data.nodes.push({name: newName,nodes: []});
		};

		$scope.saveToLocalstorage = function() {
			localStorage.setItem('recursiveObjectTree', JSON.stringify($scope.tree));
		};
	}]);