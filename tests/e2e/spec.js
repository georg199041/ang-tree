describe('angularjs Objects tree', function() {

	it('should have a title', function() {
		browser.get('http://localhost:3000/');
		expect(browser.getTitle()).toEqual('Objects tree');
	});

	it('should add element', function() {
		var button = element(by.id('add_node'));
		button.click();
		button.click();
		button.click();
		button.click();
		browser.sleep(2000);
		expect(element.all(by.css('.items>li')).count()).toBe(4)

		var button = element(by.id('add_node-1'));
		button.click();
		button.click();
		button.click();
		browser.sleep(2000);
		expect(element.all(by.css('.items ul li')).count()).toBe(3);
	});

	it('should delete elements', function() {
		browser.sleep(2000);
		expect(element.all(by.css('.items li')).count()).toBe(7);
		var button1 = element(by.id('delete_node'));
		button1.click();
		expect(element.all(by.css('.items li')).count()).toBe(0);
	});
});